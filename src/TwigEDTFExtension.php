<?php

namespace Drupal\edtf;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use EDTF\EdtfFactory;
use Drupal\edtf\Helper;

/**
 * Twig extension for working with Extended Date/Time Format (EDTF) values.
 */
class TwigEDTFExtension extends AbstractExtension {
  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {

    $filters = [
      new TwigFilter('edtf_validate', [$this, 'edtfValidate']),
      new TwigFilter('edtf_humanize', [$this, 'edtfHumanize']),
      new TwigFilter('edtf_year', [$this, 'edtfYear']),
      new TwigFilter('edtf_year_period', [$this, 'edtfYearPeriod']),
      new TwigFilter('edtf_min', [$this, 'edtfMin']),
      new TwigFilter('edtf_max', [$this, 'edtfMax']),
    ];
    
    return $filters;
  }

  /**
   * Checks the validty of an EDTF value.
   */
  public function edtfValidate (string|null $value): bool {
    $parsingResult = Helper::getParser()->parse($value ?? '');
    return $parsingResult->isValid();
  }


  /**
   * Returns an humanized version of an EDTF value.
   */
  public function edtfHumanize (string|null $value): string|null {
    $parsingResult = Helper::getParser()->parse($value ?? '');
    if (!$parsingResult->isValid()) {
      return null;
    }

    $edtfValue = $parsingResult->getEdtfValue();

    return Helper::getHumanizer()->humanize($edtfValue);
  }

  /**
   * Returns the year of an EDTF value.
   */
  public function edtfYear (string|null $value): int|null {
    $parsingResult = Helper::getParser()->parse($value ?? '');
    if (!$parsingResult->isValid()) {
      return null;
    }

    $edtfValue = $parsingResult->getEdtfValue();

    $edtfDate = Helper::fromUnixTime($edtfValue->getMin());

    return $edtfDate->getYear();
  }

  /**
   * Returns a year with 'X' in place of rightmost unspecified digits (e.g. 19xx) of an EDTF value.
   */
  public function edtfYearPeriod (string|null $value): string|null {
    $parsingResult = Helper::getParser()->parse($value ?? '');
    if (!$parsingResult->isValid()) {
      return null;
    }

    $edtfValue = $parsingResult->getEdtfValue();

    return Helper::getYearPeriod($edtfValue);
  }

  /**
   * Returns the earliest timestamp of an EDTF value.
   */
  public function edtfMin (string|null $value): int|null {
    $parsingResult = Helper::getParser()->parse($value ?? '');
    if (!$parsingResult->isValid()) {
      return null;
    }

    $edtfValue = $parsingResult->getEdtfValue();

    return $edtfValue->getMin();
  }

  /**
   * Returns the latest timestamp of an EDTF value.
   */
  public function edtfMax (string|null $value): int|null {
    $parsingResult = Helper::getParser()->parse($value ?? '');
    if (!$parsingResult->isValid()) {
      return null;
    }

    $edtfValue = $parsingResult->getEdtfValue();

    return $edtfValue->getMax();
  }
}
