<?php

namespace Drupal\edtf;

use EDTF\EdtfFactory;
use EDTF\EdtfParser;
use EDTF\Humanizer;

class Helper {
  public static $parser;
  public static $humanizers = [];

  public static function getParser (): EdtfParser {
    if (!isset(self::$parser)) {
      self::$parser = \EDTF\EdtfFactory::newParser();
    }

    return self::$parser;
  }

  public static function getHumanizer ($langcode = null): Humanizer {
    if (!$langcode) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    if (!array_key_exists($langcode, self::$humanizers)) {
      self::$humanizers[$langcode] = \EDTF\EdtfFactory::newHumanizerForLanguage($langcode);
    }

    return self::$humanizers[$langcode];
  }

  /**
   * convert a unix timestamp to an EDTF Value
   */
  public static function fromUnixTime (int $unixtime): \EDTF\Model\ExtDateTime {
    $parsingResult = self::getParser()->parse(date('Y-m-d\TH:i:s', $unixtime));

    return $parsingResult->getEdtfValue();
  }

  /**
   * Returns a year with 'X' in place of rightmost unspecified digits (e.g. 19xx) of an EDTF value.
   */
  public static function getYearPeriod ($edtfValue): string|null {
    $minYear = (string)self::fromUnixTime($edtfValue->getMin())->getYear();
    $maxYear = (string)self::fromUnixTime($edtfValue->getMax())->getYear();

    // prepend years with '0' for equal length
    $yearLen = max(strlen($minYear), strlen($maxYear));
    $minYear = str_repeat('0', $yearLen - strlen($minYear)) . $minYear;
    $maxYear = str_repeat('0', $yearLen - strlen($maxYear)) . $maxYear;

    // as long as minYear and maxYear are equal, copy characters; fill up with 'X'
    $isEqual = true;
    $result = '';
    for ($i = 0; $i < $yearLen; $i++) {
      if ($isEqual && substr($minYear, $i, 1) === substr($maxYear, $i, 1)) {
        $result .= substr($minYear, $i, 1);
      } else {
        $result .= 'X';
        $isEqual = false;
      }
    }

    return $result;
  }
}
